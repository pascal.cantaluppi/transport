﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Transport.Migrations
{
    public partial class Transport2Customer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CustomerId",
                table: "Transport",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Transport_CustomerId",
                table: "Transport",
                column: "CustomerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Transport_Customer_CustomerId",
                table: "Transport",
                column: "CustomerId",
                principalTable: "Customer",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Transport_Customer_CustomerId",
                table: "Transport");

            migrationBuilder.DropIndex(
                name: "IX_Transport_CustomerId",
                table: "Transport");

            migrationBuilder.DropColumn(
                name: "CustomerId",
                table: "Transport");
        }
    }
}
