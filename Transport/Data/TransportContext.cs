﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Transport.Models;

namespace Transport.Data
{
    public class TransportContext : DbContext
    {
        public TransportContext (DbContextOptions<TransportContext> options)
            : base(options)
        {
        }

        public DbSet<Transport.Models.Transport> Transport { get; set; } = default!;

        public DbSet<Transport.Models.Customer>? Customer { get; set; }
    }
}
