﻿using Microsoft.EntityFrameworkCore;
using Transport.Data;
using Transport.Models;
namespace Transport.Controllers;

public static class CustomerController
{
    public static void MapCustomerEndpoints (this IEndpointRouteBuilder routes)
    {
        routes.MapGet("/api/Customer", async (TransportContext db) =>
        {
            return await db.Customer.ToListAsync();
        })
        .WithName("GetAllCustomers")
        .Produces<List<Customer>>(StatusCodes.Status200OK);

        routes.MapGet("/api/Customer/{id}", async (int Id, TransportContext db) =>
        {
            return await db.Customer.FindAsync(Id)
                is Customer model
                    ? Results.Ok(model)
                    : Results.NotFound();
        })
        .WithName("GetCustomerById")
        .Produces<Customer>(StatusCodes.Status200OK)
        .Produces(StatusCodes.Status404NotFound);

        routes.MapPut("/api/Customer/{id}", async (int Id, Customer customer, TransportContext db) =>
        {
            var foundModel = await db.Customer.FindAsync(Id);

            if (foundModel is null)
            {
                return Results.NotFound();
            }

            db.Update(customer);

            await db.SaveChangesAsync();

            return Results.NoContent();
        })
        .WithName("UpdateCustomer")
        .Produces(StatusCodes.Status404NotFound)
        .Produces(StatusCodes.Status204NoContent);

        routes.MapPost("/api/Customer/", async (Customer customer, TransportContext db) =>
        {
            db.Customer.Add(customer);
            await db.SaveChangesAsync();
            return Results.Created($"/Customers/{customer.Id}", customer);
        })
        .WithName("CreateCustomer")
        .Produces<Customer>(StatusCodes.Status201Created);

        routes.MapDelete("/api/Customer/{id}", async (int Id, TransportContext db) =>
        {
            if (await db.Customer.FindAsync(Id) is Customer customer)
            {
                db.Customer.Remove(customer);
                await db.SaveChangesAsync();
                return Results.Ok(customer);
            }

            return Results.NotFound();
        })
        .WithName("DeleteCustomer")
        .Produces<Customer>(StatusCodes.Status200OK)
        .Produces(StatusCodes.Status404NotFound);
    }
}
