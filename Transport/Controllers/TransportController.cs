﻿using Microsoft.EntityFrameworkCore;
using Transport.Data;
using Transport.Models;
namespace Transport.Controllers;

public static class TransportController
{
    public static void MapTransportEndpoints (this IEndpointRouteBuilder routes)
    {
        routes.MapGet("/api/Transport", async (TransportContext db) =>
        {
            return await db.Transport.ToListAsync();
        })
        .WithName("GetAllTransports")
        .Produces<List<Models.Transport>>(StatusCodes.Status200OK);

        routes.MapGet("/api/Transport/{id}", async (int Id, TransportContext db) =>
        {
            return await db.Transport.FindAsync(Id)
                is Models.Transport model
                    ? Results.Ok(model)
                    : Results.NotFound();
        })
        .WithName("GetTransportById")
        .Produces<Models.Transport>(StatusCodes.Status200OK)
        .Produces(StatusCodes.Status404NotFound);

        routes.MapPut("/api/Transport/{id}", async (int Id, Models.Transport transport, TransportContext db) =>
        {
            var foundModel = await db.Transport.FindAsync(Id);

            if (foundModel is null)
            {
                return Results.NotFound();
            }

            db.Update(transport);

            await db.SaveChangesAsync();

            return Results.NoContent();
        })
        .WithName("UpdateTransport")
        .Produces(StatusCodes.Status404NotFound)
        .Produces(StatusCodes.Status204NoContent);

        routes.MapPost("/api/Transport/", async (Models.Transport transport, TransportContext db) =>
        {
            db.Transport.Add(transport);
            await db.SaveChangesAsync();
            return Results.Created($"/Transports/{transport.Id}", transport);
        })
        .WithName("CreateTransport")
        .Produces<Models.Transport>(StatusCodes.Status201Created);

        routes.MapDelete("/api/Transport/{id}", async (int Id, TransportContext db) =>
        {
            if (await db.Transport.FindAsync(Id) is Models.Transport transport)
            {
                db.Transport.Remove(transport);
                await db.SaveChangesAsync();
                return Results.Ok(transport);
            }

            return Results.NotFound();
        })
        .WithName("DeleteTransport")
        .Produces<Models.Transport>(StatusCodes.Status200OK)
        .Produces(StatusCodes.Status404NotFound);
    }
}
