﻿namespace Transport.Models
{
    public class Transport
    {
        public int Id { get; set; }
        public string? Code { get; set; }
        public string? Description { get; set; }
        public string? From { get; set; }
        public string? To { get; set; }
        public string? Departure { get; set; }
        public string? Arrival { get; set; }

        public int? CustomerId { get; set; }
        public virtual Customer? Customer { get; set; }
    }
}
