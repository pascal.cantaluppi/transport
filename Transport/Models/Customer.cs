﻿namespace Transport.Models
{
    public class Customer
    {
        public int Id { get; set; }
        public string? Code { get; set; }
        public string? Name { get; set; }
        public bool Active { get; set; }
    }
}
